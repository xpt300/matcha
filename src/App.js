import React, { Component } from 'react';

import Accueil from './containers/Accueil/Accueil';
import {Route, Switch} from 'react-router-dom';
import { fas } from '@fortawesome/free-solid-svg-icons'
import { library } from '@fortawesome/fontawesome-svg-core'
import Identifier from './components/Route/Identifier'

library.add(fas)

class App extends Component {
  render() {
    return (
        <Switch>
          <Route exact path='/' component={Accueil}/>
          <Route path='/' component={Identifier}/>
        </Switch>
    );
  }
}

export default App;
