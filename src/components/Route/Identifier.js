import React from 'react';

import {Route, Switch, Redirect} from 'react-router-dom';
import Profil from '../../containers/Profil/Profil';
import Layout from '../Layout/Layout';
import Reinitialisation from '../../containers/Reinitialisation/Reinitialisation';
import Confirmation from '../../containers/Confirmation/Confirmation';
import Recherche from '../../containers/Recherche/Recherche';

const identifie = () => {
    return (
        <Layout connexion='true'>
            <Switch>   
                <Route path='/user' component={Profil}/>
                <Route path='/recherche' component={Recherche}/>
                <Route path='/reinitialisation' component={Reinitialisation}/>
                <Route path='/confirmation' component={Confirmation}/>
                <Redirect to='/' />
            </Switch>
        </Layout>
    );
}

export default identifie;