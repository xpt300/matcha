import React from 'react';

import NavigationItem from './NavigationItem/NavigationItem';
import classes from './NavigationItems.css';

const NavigationItems = () => {
    return (
        <ul className={classes.NavigationItems}>
            <NavigationItem link="/">
                Yolo
            </NavigationItem>
            <NavigationItem link="/">
                XXXX
            </NavigationItem>
        </ul>
    );
};

export default NavigationItems;